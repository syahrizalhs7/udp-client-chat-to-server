﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class UDPclientok
{
    public static void Main()
    {
        byte[] data = new byte[1024];
        string input, stringData;
        UdpClient server = new UdpClient("10.252.39.186", 9050);

        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);

        string welcome = "Tersambung\n\n==================================================================\n\n";
        data = Encoding.ASCII.GetBytes(welcome);
        server.Send(data, data.Length);

        data = server.Receive(ref sender);

        {
            Console.WriteLine("Server Telah Ditemukan\n\nKetik Enter Untuk Menyambungkan", sender.ToString());
            stringData = Encoding.ASCII.GetString(data, 0, data.Length);
            Console.ReadLine();
            Console.WriteLine(stringData);

        }



        while (true)
        {
            Console.Write("Anda >> ");
            input = Console.ReadLine();
            if (input == "exit")
                break;

            server.Send(Encoding.ASCII.GetBytes(input), input.Length);
            data = server.Receive(ref sender);
            stringData = Encoding.ASCII.GetString(data, 0, data.Length);
        }
        Console.WriteLine("Stopping client");
        server.Close();
    }
}